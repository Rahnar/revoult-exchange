import React from "react";
import "./App.css";
import Header from "./Components/Header";
import Content from "./Components/Content";
import Phone from "./Components/Phone";
import Exchange from "./Containers/Exchange";

const App = () => {
  return (
    <div className="App">
      <Header />
      <Content />
      <Phone>
        <Exchange />
      </Phone>
    </div>
  );
};

export default App;
