export const ACTION_REVERSE_POCKETS = "ACTION_REVERS_POCKETS";
export const ACTION_CHANGE_INPUT_VALUE = "ACTION_CHANGE_INPUT_VALUE";
export const ACTION_UPDATE_POCKET_CURRENCY = "ACTION_UPDATE_POCKET_CURRENCY";
export const ACTION_EXCHANGE_CURRENCY = "ACTION_EXCHANGE_CURRENCY";
export const ACTION_CLEAR_INPUT_VALUE = "ACTION_CLEAR_INPUT_VALUE";
export const ACTION_OPEN_LIST_CURRENCY_MODAL =
  "ACTION_OPEN_LIST_CURRENCY_MODAL";
export const ACTION_UPDATE_TARGET_INDEX_POCKET =
  "ACTION_UPDATE_TARGET_INDEX_POCKET";

export const reverseExchangePockets = () => ({
  type: ACTION_REVERSE_POCKETS
});

export const changeInputValue = inputValuePayload => ({
  type: ACTION_CHANGE_INPUT_VALUE,
  payload: inputValuePayload
});

export const updatePocketCurrency = pocketPayload => ({
  type: ACTION_UPDATE_POCKET_CURRENCY,
  payload: pocketPayload
});

export const openListCurrencyModal = wallet => ({
  type: ACTION_OPEN_LIST_CURRENCY_MODAL,
  payload: wallet
});

export const exchangeCurrency = exchangePayload => ({
  type: ACTION_EXCHANGE_CURRENCY,
  payload: exchangePayload
});

export const updateTargetIndexPocket = index => ({
  type: ACTION_UPDATE_TARGET_INDEX_POCKET,
  payload: index
});

export const clearInputValue = () => ({
  type: ACTION_CLEAR_INPUT_VALUE
});
