import moxios from "moxios";
import { testStore } from "../utils/testMethods";
import { updateRates } from "./asyncActions";

const currencyPayload = {
  currencyNameFrom: "USD",
  currencyNameTo: "EUR"
};

describe("fetch updateRates action", () => {
  beforeEach(() => moxios.install());
  afterEach(() => moxios.uninstall());

  it("Store is update correctly", () => {
    const expectedState = {
      base: "USD",
      rates: {
        EUR: 0.8893
      }
    };

    const store = testStore();

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: expectedState
      });
    });

    return store.dispatch(updateRates(currencyPayload)).then(() => {
      const newState = store.getState();
      expect(newState.rates.from).toEqual(currencyPayload.currencyNameFrom);
      expect(newState.rates.to.name).toEqual(currencyPayload.currencyNameTo);
    });
  });
});
