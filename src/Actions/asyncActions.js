import axios from "axios";

const url = `https://api.exchangeratesapi.io/latest?`;

export const ACTION_UPDATE_RATES = "ACTION_UPDATE_RATES";
export const ACTION_UPDATE_INPUT_VALUE = "ACTION_UPDATE_INPUT_VALUE";

const MS_SECONDS = 10000;
let axiosSource;
let reqTimeOut;

export const updateRates = currencyPayload => dispatch => {
  const { currencyNameFrom, currencyNameTo } = currencyPayload;

  if (typeof axiosSource !== typeof undefined) {
    axiosSource.cancel("Operation canceled due to new request.");
    clearTimeout(reqTimeOut);
  }

  axiosSource = axios.CancelToken.source();

  return axios
    .get(`${url}&base=${currencyNameFrom}&symbols=${currencyNameTo}`, {
      cancelToken: axiosSource.token,
      timeout: 10000
    })
    .then(response => {
      const { rates } = response.data;
      const newRates = {
        from: currencyNameFrom,
        to: {
          name: currencyNameTo,
          rate: rates[currencyNameTo]
        }
      };

      dispatch({
        type: ACTION_UPDATE_INPUT_VALUE,
        payload: newRates
      });

      dispatch({
        type: ACTION_UPDATE_RATES,
        payload: newRates
      });

      reqTimeOut = setTimeout(
        () =>
          dispatch(
            updateRates({
              currencyNameFrom,
              currencyNameTo
            })
          ),
        MS_SECONDS
      );
    })
    .catch(error => {
      if (axios.isCancel(error)) {
        dispatch(
          updateRates({
            currencyNameFrom,
            currencyNameTo
          })
        );
      } else {
        console.log(error);
      }
    });
};
