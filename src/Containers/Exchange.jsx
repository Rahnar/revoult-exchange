import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import debounce from "lodash/debounce";
import { connect } from "react-redux";
import {
  getBalancePocketsCurrency,
  getNamePocketsCurrency
} from "../utils/methodsConfig";
import {
  reverseExchangePockets,
  changeInputValue,
  updatePocketCurrency,
  openListCurrencyModal,
  exchangeCurrency,
  updateTargetIndexPocket,
  clearInputValue
} from "../Actions/actions";

import { updateRates } from "../Actions/asyncActions";
import ExchangeComponent from "../Components/Exchange";

class Exchange extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isListCurrencyModalOpen: false,
      isSuccessModalOpen: false
    };

    this.reverseCurrency = debounce(this.reverseCurrency, 200);
  }

  componentDidMount() {
    const { updateRatesDispatch, rates } = this.props;
    const { from, to } = rates;

    updateRatesDispatch({
      currencyNameFrom: from,
      currencyNameTo: to.name
    });
  }

  exchangeCurrency = () => {
    const {
      exchangePockets,
      inputValue,
      exchangeCurrencyDispatch
    } = this.props;

    this.setState({
      isSuccessModalOpen: true
    });

    exchangeCurrencyDispatch({
      exchangePockets,
      inputValue
    });
  };

  closeSuccessModal = () => {
    const { clearInputValueDispatch } = this.props;
    this.setState({
      isSuccessModalOpen: false
    });

    clearInputValueDispatch();
  };

  changeCurrencyValue = e => {
    const { rates, exchangePockets, changeInputValueDispatch } = this.props;
    const targetInput = e.target;
    const { nameForm } = targetInput.dataset;
    const balance = getBalancePocketsCurrency(exchangePockets);

    changeInputValueDispatch({
      rates,
      targetInput,
      nameForm,
      ...balance
    });
  };

  reverseCurrency = () => {
    const {
      reverseExchangePocketsDispatch,
      updateRatesDispatch,
      rates
    } = this.props;
    reverseExchangePocketsDispatch();

    updateRatesDispatch({
      currencyNameFrom: rates.to.name,
      currencyNameTo: rates.from
    });
  };

  selectPocketCurrency = selectedCurrencyName => {
    const { isListCurrencyModalOpen } = this.state;
    const {
      listCurrency,
      exchangePockets,
      updateRatesDispatch,
      targetIndexPocket,
      updatePocketCurrencyDispatch
    } = this.props;
    const isSecondPocket = targetIndexPocket;
    const isSameCurrency = exchangePockets.find(
      currency => currency.name === selectedCurrencyName
    );
    let { currencyNameTo, currencyNameFrom } = getNamePocketsCurrency(
      exchangePockets
    );

    if (isSecondPocket) {
      currencyNameTo = selectedCurrencyName;
    } else {
      currencyNameFrom = selectedCurrencyName;
    }

    if (!isSameCurrency) {
      updateRatesDispatch({
        currencyNameFrom,
        currencyNameTo
      });
      updatePocketCurrencyDispatch({
        selectedCurrencyName,
        listCurrency
      });
    }

    this.setState({
      isListCurrencyModalOpen: !isListCurrencyModalOpen
    });
  };

  openListCurrencyModal = targetIndexPocket => {
    const {
      wallet,
      openListCurrencyModalDispatch,
      updateTargetIndexPocketDispatch
    } = this.props;
    const { isListCurrencyModalOpen } = this.state;

    this.setState({
      isListCurrencyModalOpen: !isListCurrencyModalOpen
    });

    updateTargetIndexPocketDispatch(targetIndexPocket);
    openListCurrencyModalDispatch(wallet);
  };

  closeListCurrencyModal = () => {
    const { isListCurrencyModalOpen } = this.state;
    this.setState({
      isListCurrencyModalOpen: !isListCurrencyModalOpen
    });
  };

  render() {
    const { isListCurrencyModalOpen, isSuccessModalOpen } = this.state;

    const {
      listCurrency,
      rates,
      exchangePockets,
      inputValue,
      isExchangeButtonDisabled
    } = this.props;

    return (
      <ExchangeComponent
        listCurrency={listCurrency}
        rates={rates}
        exchangePockets={exchangePockets}
        inputValue={inputValue}
        isExchangeButtonDisabled={isExchangeButtonDisabled}
        isListCurrencyModalOpen={isListCurrencyModalOpen}
        isSuccessModalOpen={isSuccessModalOpen}
        handleCloseSuccessModal={this.closeSuccessModal}
        handleOpenListCurrencyModal={this.openListCurrencyModal}
        handleCloseListCurrencyModal={this.closeListCurrencyModal}
        handleExchangeCurrency={this.exchangeCurrency}
        handleSelectCurrency={this.selectPocketCurrency}
        handleReverseCurrency={this.reverseCurrency}
        handleChangeCurrencyValue={this.changeCurrencyValue}
      />
    );
  }
}

Exchange.propTypes = {
  listCurrency: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        .isRequired
    })
  ).isRequired,
  wallet: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        .isRequired
    })
  ).isRequired,
  rates: PropTypes.shape({
    from: PropTypes.string.isRequired,
    to: PropTypes.shape({
      name: PropTypes.string.isRequired,
      rate: PropTypes.number.isRequired
    }).isRequired
  }).isRequired,
  exchangePockets: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        .isRequired
    })
  ).isRequired,
  inputValue: PropTypes.shape({
    from: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
    fontSize: PropTypes.shape({
      from: PropTypes.number.isRequired,
      to: PropTypes.number.isRequired
    }).isRequired
  }).isRequired,
  targetIndexPocket: PropTypes.number.isRequired,
  isExchangeButtonDisabled: PropTypes.bool.isRequired,
  updateRatesDispatch: PropTypes.func.isRequired,
  openListCurrencyModalDispatch: PropTypes.func.isRequired,
  changeInputValueDispatch: PropTypes.func.isRequired,
  updatePocketCurrencyDispatch: PropTypes.func.isRequired,
  reverseExchangePocketsDispatch: PropTypes.func.isRequired,
  exchangeCurrencyDispatch: PropTypes.func.isRequired,
  clearInputValueDispatch: PropTypes.func.isRequired,
  updateTargetIndexPocketDispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  rates: state.rates,
  wallet: state.wallet,
  listCurrency: state.listCurrency,
  inputValue: state.exchange.exchangePockets.inputValue,
  exchangePockets: state.exchange.exchangePockets.pockets,
  targetIndexPocket: state.exchange.exchangePockets.targetIndexPocket,
  isExchangeButtonDisabled: state.exchange.isExchangeButtonDisabled
});

const mapDispatchToProps = {
  updateRatesDispatch: updateRates,
  updatePocketCurrencyDispatch: updatePocketCurrency,
  updateTargetIndexPocketDispatch: updateTargetIndexPocket,
  reverseExchangePocketsDispatch: reverseExchangePockets,
  changeInputValueDispatch: changeInputValue,
  openListCurrencyModalDispatch: openListCurrencyModal,
  exchangeCurrencyDispatch: exchangeCurrency,
  clearInputValueDispatch: clearInputValue
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Exchange);
