const BASE_BIG_SIZE = 32;
const BASE_MEDIUM_SIZE = 20;
const BASE_SMALL_SIZE = 16;
const BASE_EXTRA_SMALL_SIZE = 13;

const BIG_AMOUNT_CURRENCY_LENGTH = 18;
const MEDIUM_AMOUNT_CURRENCY_LENGTH = 15;
const SMALL_AMOUNT_CURRENCY_LENGTH = 9;

export const getOptimalAmountFrontSize = amount => {
  const inputAmountValueFromLength = amount.length;
  let fontSize = 0;

  if (inputAmountValueFromLength >= SMALL_AMOUNT_CURRENCY_LENGTH) {
    fontSize = BASE_MEDIUM_SIZE;
  }

  if (inputAmountValueFromLength >= MEDIUM_AMOUNT_CURRENCY_LENGTH) {
    fontSize = BASE_SMALL_SIZE;
  }

  if (inputAmountValueFromLength >= BIG_AMOUNT_CURRENCY_LENGTH) {
    fontSize = BASE_EXTRA_SMALL_SIZE;
  }

  return inputAmountValueFromLength <= SMALL_AMOUNT_CURRENCY_LENGTH
    ? BASE_BIG_SIZE
    : fontSize;
};

export const getClearNumber = value =>
  Number(`${value}`.replace(/[^.\d]/gi, ""));

export const clearValueFromSymbols = value => `${value}`.replace(/[^,.\d]/, "");

export const getNamePocketsCurrency = currentPockets => {
  if (!Array.isArray(currentPockets) && currentPockets.length > 2)
    return currentPockets;

  return {
    currencyNameFrom: currentPockets[0].name,
    currencyNameTo: currentPockets[1].name
  };
};

export const getBalancePocketsCurrency = currentPockets => {
  if (!Array.isArray(currentPockets) && currentPockets.length > 2)
    return currentPockets;

  return {
    currencyBalanceFrom: getClearNumber(currentPockets[0].amount),
    currencyBalanceTo: getClearNumber(currentPockets[1].amount)
  };
};

export const reverseRate = rate => 1 / rate;

export const copyListCurrency = (baseArr, newArr) => {
  const arrConcat = [...baseArr, ...newArr];
  const objects = arrConcat.reduce(
    (prev, next) => ({
      ...prev,
      [next.name]: next
    }),
    {}
  );

  return Object.keys(objects).map(currency => {
    return objects[currency];
  });
};

export const addOperatorPlus = amount => `+${clearValueFromSymbols(amount)}`;
export const addOperatorMinus = amount => `-${clearValueFromSymbols(amount)}`;

export const reverseAmount = amount => {
  const { from, to } = amount;

  return {
    ...amount,
    from: to.replace(/\+/, "-"),
    to: from.replace(/-/, "+")
  };
};
