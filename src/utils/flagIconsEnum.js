import { ReactComponent as SvgIconChina } from "../assets/icons/china.svg";
import { ReactComponent as SvgIconEuropeanUnion } from "../assets/icons/european-union.svg";
import { ReactComponent as SvgIconUk } from "../assets/icons/united-kingdom.svg";
import { ReactComponent as SvgIconUsa } from "../assets/icons/united-states.svg";

export default {
  CNY: SvgIconChina,
  EUR: SvgIconEuropeanUnion,
  USD: SvgIconUsa,
  GBP: SvgIconUk
};
