const formatNumber = n => {
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export default value => {
  let inputVal = String(value);

  if (inputVal === "") {
    return;
  }

  if (inputVal.indexOf(".") >= 0) {
    const decimalPos = inputVal.indexOf(".");

    let leftSide = inputVal.substring(0, decimalPos);
    let rightSide = inputVal.substring(decimalPos);

    leftSide = formatNumber(leftSide);

    rightSide = formatNumber(rightSide);
    rightSide = rightSide.replace(/(\d),(\d)/gi, "$1$2");

    rightSide = rightSide.substring(0, 2);

    inputVal = `${leftSide}.${rightSide}`;
  } else {
    inputVal = formatNumber(inputVal);
  }

  return inputVal; // eslint-disable-line consistent-return
};
