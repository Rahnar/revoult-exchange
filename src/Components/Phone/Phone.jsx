import React from "react";
import PropTypes from "prop-types";

import css from "./Phone.module.css";

const Phone = ({ children }) => {
  return (
    <div key="PhoneComponent" className={css.phone} data-test="PhoneComponent">
      <div className={css.screen} data-test="screenWindow">
        {children}
      </div>
    </div>
  );
};

Phone.propTypes = {
  children: PropTypes.element.isRequired
};

export default Phone;
