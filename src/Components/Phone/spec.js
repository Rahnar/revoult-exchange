import React from "react";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";
import Phone from "./index";
import { findByTestAtrr, checkProps } from "../../utils/testMethods";

const setUp = (props = {}) => shallow(<Phone {...props} />);

describe("Header Component", () => {
  let component;
  const expectedProps = {
    children: React.createElement("div")
  };

  beforeEach(() => {
    component = setUp(expectedProps);
  });

  describe("Checking Props", () => {
    it("Should not throw a warning", () => {
      const propsErr = checkProps(Phone, expectedProps);
      expect(propsErr).toBeUndefined();
    });
  });

  describe("Renders", () => {
    it("Should matches the snapshot", () => {
      const tree = renderer.create(component).toJSON();
      expect(tree).toMatchSnapshot();
    });

    it("Should render without errors", () => {
      const wrapper = findByTestAtrr(component, "PhoneComponent");
      expect(wrapper.length).toBe(1);
    });

    it("Should render a screenWindow", () => {
      const wrapper = findByTestAtrr(component, "screenWindow");
      expect(wrapper.length).toBe(1);
    });
  });
});
