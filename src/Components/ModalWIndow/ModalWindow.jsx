import React from "react";
import PropTypes from "prop-types";

import classNames from "classnames";

import css from "./ModalWindow.module.css";

const ModalWindow = ({
  children,
  overlayClassName,
  modalClassName,
  modalContainerClassName,
  handleClickModalClose
}) => (
  <div className={classNames(css.modal, modalClassName)}>
    <div
      className={classNames(css.overlay, overlayClassName)}
      onClick={handleClickModalClose}
      role="none"
    />
    <div className={classNames(css.modalContainer, modalContainerClassName)}>
      {children}
    </div>
  </div>
);

ModalWindow.propTypes = {
  children: PropTypes.objectOf(PropTypes.any).isRequired,
  handleClickModalClose: PropTypes.func.isRequired,
  overlayClassName: PropTypes.string,
  modalClassName: PropTypes.string,
  modalContainerClassName: PropTypes.string
};

ModalWindow.defaultProps = {
  overlayClassName: "",
  modalClassName: "",
  modalContainerClassName: ""
};

export default ModalWindow;
