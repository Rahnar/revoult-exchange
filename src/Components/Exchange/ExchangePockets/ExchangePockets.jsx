import React from "react";
import PropTypes from "prop-types";
import currencySymbolMap from "currency-symbol-map";

import css from "./ExchangePockets.module.css";

import Input from "../../Input";
import { ReactComponent as SvgIconDownArrow } from "../../../assets/icons/down-arrow.svg";

const ExchangePockets = ({
  name,
  amount,
  index,
  inputValue,
  handleOpenListCurrencyModal,
  handleChangeCurrencyValue
}) => (
  <div className={css.container}>
    <div className={css.form}>
      <div
        className={css.currencyNameContainer}
        onClick={() => handleOpenListCurrencyModal(index)}
        role="none"
      >
        <span className={css.currencyName}>{name}</span>
        <SvgIconDownArrow className={css.downArrowIcon} />
      </div>
      <Input
        type="text"
        maxlength="17"
        className={css.inputNumber}
        style={{
          fontSize: index ? inputValue.fontSize.to : inputValue.fontSize.from
        }}
        dataNameForm={index ? "pocketAmountTo" : "pocketAmountFrom"}
        handleChange={handleChangeCurrencyValue}
        placeholder="0"
        value={index ? inputValue.to : inputValue.from}
      />
    </div>
    <span className={css.balance}>
      {`Balance: ${currencySymbolMap(name)}${amount}`}
    </span>
  </div>
);

ExchangePockets.propTypes = {
  name: PropTypes.string.isRequired,
  amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  index: PropTypes.number.isRequired,
  inputValue: PropTypes.shape({
    from: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired
  }).isRequired,
  handleOpenListCurrencyModal: PropTypes.func.isRequired,
  handleChangeCurrencyValue: PropTypes.func.isRequired
};

export default ExchangePockets;
