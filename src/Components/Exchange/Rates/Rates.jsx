import React from "react";
import PropTypes from "prop-types";
import currencySymbolMap from "currency-symbol-map";

import css from "./Rates.module.css";

const Rates = ({ from, to }) => (
  <div className={css.ratesContainer}>
    <span className={css.ratesText}>
      {`${currencySymbolMap(from)}1 = 
      ${currencySymbolMap(to.name)}${to.rate.toFixed(4)}`}
    </span>
  </div>
);

Rates.propTypes = {
  from: PropTypes.string.isRequired,
  to: PropTypes.shape({
    name: PropTypes.string.isRequired,
    rate: PropTypes.number.isRequired
  }).isRequired
};

export default Rates;
