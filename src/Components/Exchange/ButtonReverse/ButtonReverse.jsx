import React from "react";
import PropTypes from "prop-types";

import css from "./ButtonReverse.module.css";

import { ReactComponent as SvgIconArrowReverse } from "../../../assets/icons/arrow-reverse.svg";
import Button from "../../Button";

const ButtonReverse = ({ handleReverseCurrency }) => (
  <Button handleClick={handleReverseCurrency} className={css.buttonReverse}>
    <SvgIconArrowReverse width="15" height="15" />
  </Button>
);

ButtonReverse.propTypes = {
  handleReverseCurrency: PropTypes.func.isRequired
};

export default ButtonReverse;
