import React from "react";
import PropTypes from "prop-types";

import css from "./Exchange.module.css";

import ExchangePockets from "./ExchangePockets";
import Button from "../Button";
import ListCurrencyModal from "./CurrencyModal";
import ButtonReverse from "./ButtonReverse";
import Rates from "./Rates";
import SuccessExchangeModal from "./SuccessExchangeModal";

const Exchange = ({
  listCurrency,
  rates,
  exchangePockets,
  inputValue,
  isExchangeButtonDisabled,
  isListCurrencyModalOpen,
  isSuccessModalOpen,
  handleOpenListCurrencyModal,
  handleCloseListCurrencyModal,
  handleExchangeCurrency,
  handleSelectCurrency,
  handleReverseCurrency,
  handleChangeCurrencyValue,
  handleCloseSuccessModal
}) => (
  <div className={css.container} data-test="ExchangeComponent">
    <header className={css.header} data-test="header">
      Exchange
    </header>
    {exchangePockets.map((currency, index) => (
      <ExchangePockets
        key={currency.name}
        {...currency}
        index={index}
        inputValue={inputValue}
        isListCurrencyModalOpen={isListCurrencyModalOpen}
        handleOpenListCurrencyModal={handleOpenListCurrencyModal}
        handleChangeCurrencyValue={handleChangeCurrencyValue}
      />
    ))}
    <ButtonReverse handleReverseCurrency={handleReverseCurrency} />

    <Button
      handleClick={handleExchangeCurrency}
      className={css.exchangeButton}
      disabled={isExchangeButtonDisabled}
    >
      <span>Exchange</span>
    </Button>

    <Rates {...rates} />

    {isListCurrencyModalOpen && (
      <ListCurrencyModal
        listCurrency={listCurrency}
        handleSelectCurrency={handleSelectCurrency}
        handleCloseModalWindow={handleCloseListCurrencyModal}
      />
    )}

    {isSuccessModalOpen && (
      <SuccessExchangeModal
        {...inputValue}
        exchangePockets={exchangePockets}
        handleCloseSuccessModal={handleCloseSuccessModal}
      />
    )}
  </div>
);

Exchange.propTypes = {
  listCurrency: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        .isRequired
    })
  ).isRequired,
  rates: PropTypes.shape({
    from: PropTypes.string.isRequired,
    to: PropTypes.shape({
      name: PropTypes.string.isRequired,
      rate: PropTypes.number.isRequired
    }).isRequired
  }).isRequired,
  exchangePockets: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        .isRequired
    })
  ).isRequired,
  inputValue: PropTypes.shape({
    from: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
    fontSize: PropTypes.shape({
      from: PropTypes.number.isRequired,
      to: PropTypes.number.isRequired
    }).isRequired
  }).isRequired,
  isExchangeButtonDisabled: PropTypes.bool,
  isListCurrencyModalOpen: PropTypes.bool.isRequired,
  isSuccessModalOpen: PropTypes.bool.isRequired,
  handleOpenListCurrencyModal: PropTypes.func.isRequired,
  handleCloseListCurrencyModal: PropTypes.func.isRequired,
  handleExchangeCurrency: PropTypes.func.isRequired,
  handleSelectCurrency: PropTypes.func.isRequired,
  handleReverseCurrency: PropTypes.func.isRequired,
  handleChangeCurrencyValue: PropTypes.func.isRequired,
  handleCloseSuccessModal: PropTypes.func.isRequired
};

Exchange.defaultProps = {
  isExchangeButtonDisabled: false
};

export default Exchange;
