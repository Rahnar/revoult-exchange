import React from "react";
import PropTypes from "prop-types";
import currencySymbolMap from "currency-symbol-map";

import { ReactComponent as SvgIconSuccess } from "../../../assets/icons/success.svg";

import css from "./SuccessExchangeModal.module.css";

import ModalWindow from "../../ModalWIndow";
import Button from "../../Button";
import {
  getClearNumber,
  getNamePocketsCurrency
} from "../../../utils/methodsConfig";

const SuccessExchangeModal = ({
  exchangePockets,
  handleCloseSuccessModal,
  from,
  to
}) => {
  const { currencyNameFrom, currencyNameTo } = getNamePocketsCurrency(
    exchangePockets
  );
  return (
    <ModalWindow
      handleClickModalClose={handleCloseSuccessModal}
      modalContainerClassName={css.containerWindowModal}
      overlayClassName={css.overlayModalWindow}
    >
      <div className={css.container}>
        <SvgIconSuccess
          id="successAnimation"
          className="animated"
          widht="120px"
          height="120px"
        />
        <span className={css.message}>
          {`You exchange ${currencySymbolMap(currencyNameFrom)}${getClearNumber(
            from
          )} to ${currencySymbolMap(currencyNameTo)}${getClearNumber(to)}`}
        </span>
        <Button handleClick={handleCloseSuccessModal} className={css.button}>
          <>Done</>
        </Button>
      </div>
    </ModalWindow>
  );
};

SuccessExchangeModal.propTypes = {
  from: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
  exchangePockets: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        .isRequired
    })
  ).isRequired,
  handleCloseSuccessModal: PropTypes.func.isRequired
};

export default SuccessExchangeModal;
