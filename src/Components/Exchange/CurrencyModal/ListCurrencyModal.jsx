import React from "react";
import PropTypes from "prop-types";

import css from "./ListCurrencyModal.module.css";

import Button from "../../Button";
import ModalWindow from "../../ModalWIndow";
import ListCurrency from "../../ListCurrency";

const ListCurrencyModal = ({
  listCurrency,
  handleCloseModalWindow,
  handleSelectCurrency
}) => (
  <ModalWindow
    handleClickModalClose={handleCloseModalWindow}
    modalContainerClassName={css.modalContainerWindowModal}
    modalClassName={css.modalWindow}
  >
    <>
      <div className={css.container}>
        <header className={css.header}>Choose currency:</header>
        <ListCurrency
          listCurrency={listCurrency}
          handleSelectCurrency={handleSelectCurrency}
        />
      </div>
      <Button handleClick={handleCloseModalWindow} className={css.buttonCancel}>
        <span>Cancel</span>
      </Button>
    </>
  </ModalWindow>
);

ListCurrencyModal.propTypes = {
  listCurrency: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        .isRequired
    })
  ).isRequired,
  handleSelectCurrency: PropTypes.func.isRequired,
  handleCloseModalWindow: PropTypes.func.isRequired
};

export default ListCurrencyModal;
