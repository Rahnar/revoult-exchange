import React from "react";
import PropTypes from "prop-types";

import ListCurrencyItem from "./ListCurrencyItem";

const ListCurrency = ({ listCurrency, handleSelectCurrency }) => (
  <div>
    {listCurrency.map(currency => (
      <ListCurrencyItem
        {...currency}
        handleSelectCurrency={handleSelectCurrency}
        key={`${currency.name}${currency.balance}`}
      />
    ))}
  </div>
);

ListCurrency.propTypes = {
  listCurrency: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        .isRequired
    })
  ).isRequired,
  handleSelectCurrency: PropTypes.func.isRequired
};

export default ListCurrency;
