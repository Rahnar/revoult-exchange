import React from "react";
import PropTypes from "prop-types";

import flagIconsEnum from "../../utils/flagIconsEnum";

import css from "./ListCurrencyItem.module.css";

const ListCurrencyItem = ({ name, amount, handleSelectCurrency }) => {
  const FlagIconComponent = flagIconsEnum[name];

  return (
    <div
      className={css.currencyItem}
      onClick={() => handleSelectCurrency(name)}
      role="none"
    >
      <FlagIconComponent width="30" height="30" className={css.flagIcon} />
      <span className={css.currencyInfo}>{`${name} - ${amount}`}</span>
    </div>
  );
};

ListCurrencyItem.propTypes = {
  name: PropTypes.string.isRequired,
  amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  handleSelectCurrency: PropTypes.func.isRequired
};

export default ListCurrencyItem;
