import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import css from "./Button.module.css";

const Button = ({ className, children, handleClick, disabled }) => (
  <button
    className={classNames(css.button, className)}
    type="button"
    disabled={disabled}
    onClick={handleClick}
  >
    {children}
  </button>
);

Button.propTypes = {
  className: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  children: PropTypes.objectOf(PropTypes.any).isRequired
};

Button.defaultProps = {
  disabled: false
};

export default Button;
