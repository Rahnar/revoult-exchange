import React from "react";
import PropTypes from "prop-types";

const Input = ({
  className,
  type,
  placeholder,
  handleChange,
  maxlength,
  value,
  dataNameForm,
  style
}) => (
  <input
    type={type}
    className={className}
    onChange={handleChange}
    placeholder={placeholder}
    maxLength={maxlength}
    value={value}
    style={style}
    data-name-form={dataNameForm}
  />
);

Input.propTypes = {
  className: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  maxlength: PropTypes.string,
  value: PropTypes.string.isRequired,
  dataNameForm: PropTypes.string.isRequired,
  style: PropTypes.shape({
    fontSize: PropTypes.number.isRequired
  }).isRequired
};

Input.defaultProps = {
  placeholder: "",
  maxlength: ""
};

export default Input;
