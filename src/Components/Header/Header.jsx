import React from "react";
import css from "./Header.module.css";
import { ReactComponent as SvgIconLogo } from "../../assets/icons/logo.svg";

const Header = () => {
  return (
    <header
      key="HeaderComponent"
      className={css.header}
      data-test="HeaderComponent"
    >
      <SvgIconLogo width="104" heught="84" data-test="svgLogo" />
    </header>
  );
};

export default Header;
