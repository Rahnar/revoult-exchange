import React from "react";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";
import Header from "./index";
import { findByTestAtrr } from "../../utils/testMethods";

const setUp = (props = {}) => shallow(<Header {...props} />);

describe("Header Component", () => {
  let component;

  beforeEach(() => {
    component = setUp();
  });

  it("Should matches the snapshot", () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("Should render without errors", () => {
    const wrapper = findByTestAtrr(component, "HeaderComponent");
    expect(wrapper.length).toBe(1);
  });

  it("Should render a logo", () => {
    const wrapper = findByTestAtrr(component, "svgLogo");
    expect(wrapper.length).toBe(1);
  });
});
