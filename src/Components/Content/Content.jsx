import React from "react";
import css from "./Content.module.css";

const Content = () => {
  return (
    <section
      key="ContentComponent"
      className={css.container}
      data-test="ContentComponent"
    >
      <h1 className={css.title} data-test="title">
        A Radically Better Exchange
      </h1>
      <p className={css.text} data-test="text">
        Banks will charge you when you spend or transfer money abroad. We’re not
        about that, and that’s why over four million people have switched to
        Revolut.
      </p>
    </section>
  );
};

export default Content;
