import React from "react";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";
import Content from "./index";
import { findByTestAtrr } from "../../utils/testMethods";

const setUp = (props = { key: 1 }) => shallow(<Content {...props} />);

describe("Header Component", () => {
  let component;

  beforeEach(() => {
    component = setUp();
  });

  it("Should matches the snapshot", () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("Should render without errors", () => {
    const wrapper = findByTestAtrr(component, "ContentComponent");
    expect(wrapper.length).toBe(1);
  });

  it("Should render a title", () => {
    const wrapper = findByTestAtrr(component, "title");
    expect(wrapper.length).toBe(1);
  });

  it("Should render a text", () => {
    const wrapper = findByTestAtrr(component, "text");
    expect(wrapper.length).toBe(1);
  });
});
