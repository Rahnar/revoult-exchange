import {
  ACTION_CHANGE_INPUT_VALUE,
  ACTION_REVERSE_POCKETS,
  ACTION_UPDATE_POCKET_CURRENCY,
  ACTION_UPDATE_TARGET_INDEX_POCKET,
  ACTION_CLEAR_INPUT_VALUE
} from "../Actions/actions";
import { ACTION_UPDATE_INPUT_VALUE } from "../Actions/asyncActions";

import exchangeReducer from "./exchange";

const defaultState = {
  exchangePockets: {
    pockets: [
      {
        name: "GBP",
        amount: 300
      },
      {
        name: "EUR",
        amount: 100
      }
    ],
    inputValue: {
      from: "",
      to: "",
      fontSize: {
        from: 32,
        to: 32
      }
    },
    targetIndexPocket: 0
  },
  isExchangeButtonDisabled: true
};

describe("Exchange Reducer", () => {
  it("Should return default state", () => {
    const newState = exchangeReducer(undefined, {});
    expect(defaultState).toEqual(newState);
  });

  it("Should return new state if action REVERSE_POCKETS", () => {
    const expectData = {
      ...defaultState,
      exchangePockets: {
        ...defaultState.exchangePockets,
        pockets: [
          {
            name: "EUR",
            amount: 100
          },
          {
            name: "GBP",
            amount: 300
          }
        ]
      }
    };

    const newState = exchangeReducer(undefined, {
      type: ACTION_REVERSE_POCKETS
    });

    expect(expectData).toEqual(newState);
  });

  it("Should return new state if action ACTION_UPDATE_POCKET_CURRENCY", () => {
    const payload = {
      listCurrency: [
        {
          name: "USD",
          amount: 100
        },
        {
          name: "EUR",
          amount: 300
        }
      ],
      selectedCurrencyName: "USD"
    };

    const expectData = {
      ...defaultState,
      exchangePockets: {
        ...defaultState.exchangePockets,
        pockets: [
          {
            name: "USD",
            amount: 100
          },
          {
            name: "EUR",
            amount: 100
          }
        ]
      }
    };

    const newState = exchangeReducer(undefined, {
      type: ACTION_UPDATE_POCKET_CURRENCY,
      payload
    });

    expect(expectData).toEqual(newState);
  });

  it("Should return new state if action ACTION_UPDATE_INPUT_VALUE", () => {
    const payload = {
      to: {
        rate: 1.156107
      }
    };

    const newState = exchangeReducer(undefined, {
      type: ACTION_UPDATE_INPUT_VALUE,
      payload
    });

    expect(defaultState).toEqual(newState);
  });

  it("Should return new state if action ACTION_CHANGE_INPUT_VALUE", () => {
    const payload = {
      rates: {
        from: "GBP",
        to: {
          name: "EUR",
          rate: 1.156107
        }
      },
      nameForm: "pocketAmountFrom",
      targetInput: {
        value: "1000"
      }
    };

    const expectData = {
      ...defaultState,
      exchangePockets: {
        ...defaultState.exchangePockets,
        inputValue: {
          ...defaultState.exchangePockets.inputValue,
          from: "-1,000",
          to: "+1,156.10"
        }
      }
    };

    const newState = exchangeReducer(undefined, {
      type: ACTION_CHANGE_INPUT_VALUE,
      payload
    });

    expect(expectData).toEqual(newState);
  });

  it("Should return new state if action ACTION_UPDATE_TARGET_INDEX_POCKET", () => {
    const payload = 1;

    const expectData = {
      ...defaultState,
      exchangePockets: {
        ...defaultState.exchangePockets,
        targetIndexPocket: 1
      }
    };

    const newState = exchangeReducer(undefined, {
      type: ACTION_UPDATE_TARGET_INDEX_POCKET,
      payload
    });

    expect(expectData).toEqual(newState);
  });

  it("Should return new state if action ACTION_CLEAR_INPUT_VALUE", () => {
    const newState = exchangeReducer(undefined, {
      type: ACTION_CLEAR_INPUT_VALUE
    });

    expect(defaultState).toEqual(newState);
  });
});
