import { ACTION_OPEN_LIST_CURRENCY_MODAL } from "../Actions/actions";

import listCurrencyReducer from "./listCurrency";

describe("Exchange Reducer", () => {
  const defaultState = [
    {
      name: "GBP",
      amount: 0
    },
    {
      name: "EUR",
      amount: 0
    },
    {
      name: "USD",
      amount: 0
    },
    {
      name: "CNY",
      amount: 0
    }
  ];

  it("Should return default state", () => {
    const newState = listCurrencyReducer(undefined, {});
    expect(defaultState).toEqual(newState);
  });

  it("Should return new state if action ACTION_OPEN_LIST_CURRENCY_MODAL", () => {
    const payload = [
      {
        name: "GBP",
        amount: 100
      }
    ];

    const expectData = [
      {
        name: "GBP",
        amount: 100
      },
      {
        name: "EUR",
        amount: 0
      },
      {
        name: "USD",
        amount: 0
      },
      {
        name: "CNY",
        amount: 0
      }
    ];

    const newState = listCurrencyReducer(undefined, {
      type: ACTION_OPEN_LIST_CURRENCY_MODAL,
      payload
    });

    expect(expectData).toEqual(newState);
  });
});
