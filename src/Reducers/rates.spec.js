import ratesReducer from "./rates";
import { ACTION_UPDATE_RATES } from "../Actions/asyncActions";

describe("Exchange Reducer", () => {
  const defaultState = {
    from: "GBP",
    to: {
      name: "EUR",
      rate: 1.156107
    }
  };

  it("Should return default state", () => {
    const newState = ratesReducer(undefined, {});
    expect(defaultState).toEqual(newState);
  });

  it("Should return new state if action ACTION_UPDATE_RATES", () => {
    const payload = {
      from: "USD",
      to: {
        name: "EUR",
        rate: 0.8643
      }
    };

    const expectData = {
      from: "USD",
      to: {
        name: "EUR",
        rate: 0.8643
      }
    };

    const newState = ratesReducer(undefined, {
      type: ACTION_UPDATE_RATES,
      payload
    });

    expect(expectData).toEqual(newState);
  });
});
