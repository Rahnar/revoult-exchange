import formatCurrency from "../utils/formatCurrency";

import { ACTION_EXCHANGE_CURRENCY } from "../Actions/actions";
import {
  copyListCurrency,
  getBalancePocketsCurrency,
  getClearNumber
} from "../utils/methodsConfig";

const initialState = [
  {
    name: "GBP",
    amount: 300
  },
  {
    name: "EUR",
    amount: 100
  },
  {
    name: "USD",
    amount: 50
  }
];

export default (state = initialState, action) => {
  let newState;

  switch (action.type) {
    case ACTION_EXCHANGE_CURRENCY:
      {
        const { exchangePockets, inputValue } = action.payload;
        const newExchangePockets = [...exchangePockets];
        const {
          currencyBalanceFrom,
          currencyBalanceTo
        } = getBalancePocketsCurrency(exchangePockets);

        const newBalance = {
          balanceFrom: currencyBalanceFrom - getClearNumber(inputValue.from, 2),
          balanceTo: currencyBalanceTo + getClearNumber(inputValue.to)
        };
        const { balanceFrom, balanceTo } = newBalance;

        newExchangePockets[0].amount = formatCurrency(balanceFrom);
        newExchangePockets[1].amount = formatCurrency(balanceTo);

        newState = copyListCurrency(state, exchangePockets);
      }
      break;
    default:
      newState = state;
  }
  return newState;
};
