import { ACTION_EXCHANGE_CURRENCY } from "../Actions/actions";

import walletReducer from "./wallet";

describe("Exchange Reducer", () => {
  const defaultState = [
    {
      name: "GBP",
      amount: 300
    },
    {
      name: "EUR",
      amount: 100
    },
    {
      name: "USD",
      amount: 50
    }
  ];

  it("Should return default state", () => {
    const newState = walletReducer(undefined, {});
    expect(defaultState).toEqual(newState);
  });

  it("Should return new state if action ACTION_EXCHANGE_CURRENCY", () => {
    const payload = {
      inputValue: {
        from: 100,
        to: 1000
      },
      exchangePockets: [
        {
          name: "GBP",
          amount: 300
        },
        {
          name: "CNY",
          amount: 0
        }
      ]
    };

    const expectData = [
      {
        name: "GBP",
        amount: "200"
      },
      {
        name: "EUR",
        amount: 100
      },
      {
        name: "USD",
        amount: 50
      },
      {
        name: "CNY",
        amount: "1,000"
      }
    ];

    const newState = walletReducer(undefined, {
      type: ACTION_EXCHANGE_CURRENCY,
      payload
    });

    expect(expectData).toEqual(newState);
  });
});
