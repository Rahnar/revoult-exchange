import formatCurrency from "../utils/formatCurrency";
import {
  reverseRate,
  getClearNumber,
  getBalancePocketsCurrency,
  reverseAmount,
  getOptimalAmountFrontSize
} from "../utils/methodsConfig";

import {
  ACTION_CHANGE_INPUT_VALUE,
  ACTION_CLEAR_INPUT_VALUE,
  ACTION_REVERSE_POCKETS,
  ACTION_UPDATE_POCKET_CURRENCY,
  ACTION_UPDATE_TARGET_INDEX_POCKET
} from "../Actions/actions";
import { ACTION_UPDATE_INPUT_VALUE } from "../Actions/asyncActions";

const POCKET_AMOUNT_FROM = "pocketAmountFrom";
const POCKET_AMOUNT_TO = "pocketAmountTo";

const initialState = {
  exchangePockets: {
    pockets: [
      {
        name: "GBP",
        amount: 300
      },
      {
        name: "EUR",
        amount: 100
      }
    ],
    inputValue: {
      from: "",
      to: "",
      fontSize: {
        from: 32,
        to: 32
      }
    },
    targetIndexPocket: 0
  },
  isExchangeButtonDisabled: true
};

export default (state = initialState, action) => {
  let newState;

  switch (action.type) {
    case ACTION_REVERSE_POCKETS:
      {
        const { pockets, inputValue } = state.exchangePockets;
        const { from, to } = reverseAmount(inputValue);
        const newPockets = [...pockets].reverse();
        const { currencyBalanceFrom } = getBalancePocketsCurrency(newPockets);
        const inputValueInFirstPocket = getClearNumber(from);

        newState = {
          ...state,
          exchangePockets: {
            ...state.exchangePockets,
            pockets: newPockets,
            inputValue: {
              from,
              to,
              fontSize: {
                from: getOptimalAmountFrontSize(from),
                to: getOptimalAmountFrontSize(to)
              }
            }
          },
          isExchangeButtonDisabled:
            inputValueInFirstPocket > currencyBalanceFrom ||
            !inputValueInFirstPocket
        };
      }
      break;
    case ACTION_UPDATE_POCKET_CURRENCY:
      {
        const { listCurrency, selectedCurrencyName } = action.payload;
        const { pockets, targetIndexPocket } = state.exchangePockets;
        const newPockets = [...pockets];
        const selectedCurrency = listCurrency.find(
          currency => currency.name === selectedCurrencyName
        );

        newPockets[targetIndexPocket] = selectedCurrency;

        newState = {
          ...state,
          exchangePockets: {
            ...state.exchangePockets,
            pockets: newPockets
          }
        };
      }
      break;
    case ACTION_UPDATE_INPUT_VALUE:
      {
        const { inputValue } = state.exchangePockets;
        const { rate } = action.payload.to;
        const { currencyBalanceFrom } = getBalancePocketsCurrency(
          state.exchangePockets.pockets
        );
        let newFromValue = inputValue.from;
        let newToValue = inputValue.to;
        let isExchangeButtonDisabled = true;

        if (newFromValue || newToValue) {
          newFromValue = getClearNumber(inputValue.from);
          newToValue = getClearNumber(inputValue.from) * rate;

          isExchangeButtonDisabled =
            getClearNumber(newFromValue) > getClearNumber(currencyBalanceFrom);

          newFromValue = `-${formatCurrency(newFromValue)}`;
          newToValue = `+${formatCurrency(newToValue)}`;
        }

        newState = {
          ...state,
          exchangePockets: {
            ...state.exchangePockets,
            inputValue: {
              from: newFromValue,
              to: newToValue,
              fontSize: {
                from: getOptimalAmountFrontSize(newFromValue),
                to: getOptimalAmountFrontSize(newToValue)
              }
            }
          },
          isExchangeButtonDisabled
        };
      }
      break;
    case ACTION_CHANGE_INPUT_VALUE:
      {
        const { rates, targetInput, nameForm } = action.payload;
        const currencyBalanceFrom = state.exchangePockets.pockets[0].amount;
        const formatValue = formatCurrency(targetInput.value);
        const formatNumber = getClearNumber(formatValue);
        let inputAmountValueFrom = formatValue;
        let inputAmountValueTo = formatValue;

        if (nameForm === POCKET_AMOUNT_FROM) {
          inputAmountValueTo = rates.to.rate * formatNumber;
        } else if (nameForm === POCKET_AMOUNT_TO) {
          inputAmountValueFrom = reverseRate(rates.to.rate) * formatNumber;
        }

        const isExchangeButtonDisabled =
          getClearNumber(inputAmountValueFrom) >
            getClearNumber(currencyBalanceFrom) || !formatNumber;

        inputAmountValueFrom = `-${formatCurrency(inputAmountValueFrom)}`;
        inputAmountValueTo = `+${formatCurrency(inputAmountValueTo)}`;

        newState = {
          ...state,
          exchangePockets: {
            ...state.exchangePockets,
            inputValue: {
              fontSize: {
                from: getOptimalAmountFrontSize(inputAmountValueFrom),
                to: getOptimalAmountFrontSize(inputAmountValueTo)
              },
              from: formatNumber ? inputAmountValueFrom : "",
              to: formatNumber ? inputAmountValueTo : ""
            }
          },
          isExchangeButtonDisabled
        };
      }
      break;
    case ACTION_UPDATE_TARGET_INDEX_POCKET:
      newState = {
        ...state,
        exchangePockets: {
          ...state.exchangePockets,
          targetIndexPocket: action.payload
        }
      };
      break;
    case ACTION_CLEAR_INPUT_VALUE:
      newState = {
        ...state,
        exchangePockets: {
          ...state.exchangePockets,
          inputValue: {
            ...state.exchangePockets.inputValue,
            from: "",
            to: ""
          }
        },
        isExchangeButtonDisabled: true
      };
      break;
    default:
      newState = state;
  }

  return newState;
};
