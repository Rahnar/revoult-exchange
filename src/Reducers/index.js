import { combineReducers } from "redux";
import listCurrency from "./listCurrency";
import exchange from "./exchange";
import wallet from "./wallet";
import rates from "./rates";

export default combineReducers({
  listCurrency,
  wallet,
  exchange,
  rates
});
