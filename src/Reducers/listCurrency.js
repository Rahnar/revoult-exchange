import { ACTION_OPEN_LIST_CURRENCY_MODAL } from "../Actions/actions";
import { copyListCurrency } from "../utils/methodsConfig";

const initialState = [
  {
    name: "GBP",
    amount: 0
  },
  {
    name: "EUR",
    amount: 0
  },
  {
    name: "USD",
    amount: 0
  },
  {
    name: "CNY",
    amount: 0
  }
];

export default (state = initialState, action) => {
  let newState;

  switch (action.type) {
    case ACTION_OPEN_LIST_CURRENCY_MODAL:
      newState = copyListCurrency(state, action.payload);
      break;
    default:
      newState = state;
  }

  return newState;
};
