import { ACTION_UPDATE_RATES } from "../Actions/asyncActions";

const initialState = {
  from: "GBP",
  to: {
    name: "EUR",
    rate: 1.156107
  }
};

export default (state = initialState, action) => {
  let newState;

  switch (action.type) {
    case ACTION_UPDATE_RATES:
      newState = {
        ...state,
        ...action.payload
      };
      break;
    default:
      newState = state;
  }

  return newState;
};
